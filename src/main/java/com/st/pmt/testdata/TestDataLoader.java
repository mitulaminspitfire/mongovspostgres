package com.st.pmt.testdata;

import java.io.InputStreamReader;
import java.util.List;
import org.springframework.stereotype.Component;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

@Component
public class TestDataLoader {
	public List<TestDataBean> loadTestData() {
		final ColumnPositionMappingStrategy<TestDataBean> strategy = new ColumnPositionMappingStrategy<TestDataBean>();
		strategy.setType(TestDataBean.class);
		final String[] columns = new String[] { "firstName", "lastName", "companyName", "address", "city", "county", "state", "zip", "phone1", "phone2", "email", "web" };
		strategy.setColumnMapping(columns);

		final CsvToBean<TestDataBean> csv = new CsvToBean<TestDataBean>();

		final CSVReader reader = new CSVReader(new InputStreamReader(this.getClass().getResourceAsStream("/testdata.csv")), ';', '"', 1);

		final List<TestDataBean> list = csv.parse(strategy, reader);

		return list;
	}
}
