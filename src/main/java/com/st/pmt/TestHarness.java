package com.st.pmt;

import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.st.pmt.testdata.TestDataBean;
import com.st.pmt.testdata.TestDataLoader;
import com.st.pmt.tests.InsertTest;

@Configuration
@ComponentScan("com.st.pmt")
public class TestHarness {

	public static void main(String[] args) {
		final TestHarness harness = new TestHarness();
		final ApplicationContext context = new AnnotationConfigApplicationContext(TestHarness.class);

		final TestDataLoader loader = context.getBean(TestDataLoader.class);
		final List<TestDataBean> testData = loader.loadTestData();

		final InsertTest insertTest = context.getBean(InsertTest.class);
		insertTest.insertTestContacts(testData);
	}
}
