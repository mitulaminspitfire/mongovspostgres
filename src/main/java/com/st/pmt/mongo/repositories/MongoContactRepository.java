package com.st.pmt.mongo.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.st.pmt.mongo.model.MongoContact;

@Repository
public interface MongoContactRepository extends PagingAndSortingRepository<MongoContact, String> {
}
