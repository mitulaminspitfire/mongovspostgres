package com.st.pmt.tests;

import static com.st.pmt.utils.TestingUtils.nanoToSecondsString;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.st.pmt.mongo.model.MongoContact;
import com.st.pmt.mongo.repositories.MongoContactRepository;
import com.st.pmt.postgres.model.PostgresContact;
import com.st.pmt.postgres.repositories.PostgresContactRepository;
import com.st.pmt.testdata.TestDataBean;

@Service
public class InsertTest {
	private static final Logger	LOG	= Logger.getLogger(InsertTest.class.getName());

	@Autowired
	MongoContactRepository			mongoContactRepository;

	@Autowired
	PostgresContactRepository		postgresContactRepository;

	public void insertTestContacts(List<TestDataBean> testData) {
		final long startTime = System.nanoTime();
		insertTestContactsInMongo(testData);
		final long mongoEndTime = System.nanoTime();
		insertTestContactsInPostgres(testData);
		final long postgresEndTime = System.nanoTime();

		LOG.info("Insert Test. Number of contacts = " + testData.size());

		final long totalMongoTime = mongoEndTime - startTime;
		final long totalPostgresTime = postgresEndTime - mongoEndTime;

		final String winner = totalMongoTime < totalPostgresTime ? "MongoDB" : "PostgreSql";
		long timeDiff = totalMongoTime - totalPostgresTime;
		if (timeDiff < 0) {
			timeDiff *= -1;
		}

		LOG.info(String.format("Mongo: %s, Postgres: %s, winner: %s by %s",
				nanoToSecondsString(totalMongoTime),
				nanoToSecondsString(totalPostgresTime),
				winner,
				nanoToSecondsString(timeDiff)));
	}

	private void insertTestContactsInMongo(List<TestDataBean> testData) {
		for (final TestDataBean bean : testData) {

			final MongoContact contact = new MongoContact();
			contact.setFirstName(bean.getFirstName());
			contact.setLastName(bean.getLastName());
			contact.setEmail(bean.getEmail());

			mongoContactRepository.save(contact);
		}
	}

	private void insertTestContactsInPostgres(List<TestDataBean> testData) {
		for (final TestDataBean bean : testData) {

			final PostgresContact contact = new PostgresContact();
			contact.setFirstName(bean.getFirstName());
			contact.setLastName(bean.getLastName());
			contact.setEmail(bean.getEmail());

			postgresContactRepository.save(contact);
		}
	}
}
