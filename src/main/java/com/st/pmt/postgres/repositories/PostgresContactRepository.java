package com.st.pmt.postgres.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.st.pmt.postgres.model.PostgresContact;

@Repository
public interface PostgresContactRepository extends PagingAndSortingRepository<PostgresContact, Integer> {

}
