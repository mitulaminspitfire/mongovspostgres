package com.st.pmt.postgres.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "company")
@Data
@EqualsAndHashCode(callSuper = false, of = { "companyId" })
@NoArgsConstructor
public class PostgresCompany extends BaseEntity {

	@Id
	@Column(name = "company_id", unique = true, nullable = false)
	@GeneratedValue
	private Integer	companyId;

	@Column(name = "name", length = 128, nullable = true)
	private String	name;
}