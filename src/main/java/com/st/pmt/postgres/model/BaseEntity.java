package com.st.pmt.postgres.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class BaseEntity {

	@Column(name = "created_on", updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	protected Date	createdOn;

	@Column(name = "modified_on")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date	modifiedOn;

	public Date getCreatedOn() {
		return createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	@PrePersist
	protected void preCreate() {
		createdOn = new Date();
		modifiedOn = new Date();
	}

	@PreUpdate
	protected void preUpdate() {
		modifiedOn = new Date();
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
}
