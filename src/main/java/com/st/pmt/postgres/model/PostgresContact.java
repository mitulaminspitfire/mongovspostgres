package com.st.pmt.postgres.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Entity
@Component
@Table(name = "contact")
@Data
@EqualsAndHashCode(callSuper = false, of = { "contactId" })
@NoArgsConstructor
public class PostgresContact extends BaseEntity {

	@Id
	@Column(name = "contact_id", unique = true, nullable = false)
	@GeneratedValue
	private Integer					contactId;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "company_id")
	private PostgresCompany	company;

	@Column(name = "first_name", length = 128, nullable = false)
	private String					firstName;

	@Column(name = "last_name", length = 128, nullable = true)
	private String					lastName;

	@Column(name = "email", length = 128, nullable = true)
	private String					email;
}
