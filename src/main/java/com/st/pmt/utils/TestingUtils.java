package com.st.pmt.utils;

public class TestingUtils {
	public static double nanoToSeconds(long nanoTime) {
		if (nanoTime == 0)
			return 0;

		return (double) nanoTime / 1000000000;
	}

	public static String nanoToSecondsString(long nanoTime) {
		return String.format("%.4fs", nanoToSeconds(nanoTime));
	}
}
